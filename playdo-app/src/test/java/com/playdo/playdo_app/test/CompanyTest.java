package com.playdo.playdo_app.test;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.playdo.playdo_app.model.Company;


public class CompanyTest {

	Company company;
	
	@Before
	public void setup() {
		company = new Company();
	}

	@Test
	public void testGettersAndSetters() {
		company.setCompanyId(1);
		assertEquals(1, company.getCompanyId());
		
		company.setCompanyName("CompanyName");
		assertEquals("CompanyName", company.getCompanyName());
		

	}
}
