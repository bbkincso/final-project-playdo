var month_names = ["Jan", "Feb", "Mar", 
    "Apr", "May", "Jun", "Jul", "Aug", "Sep", 
    "Oct", "Nov", "Dec"];
    
var today = new Date();
var day = today.getDate();
var month_index = today.getMonth();
var year = today.getFullYear();


$(document).ready(function(){
	
	$('.date').text(day + "/" + month_names[month_index] + "/" + year);
});