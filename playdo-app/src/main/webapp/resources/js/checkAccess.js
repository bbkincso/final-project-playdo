var rootURL2 = "http://localhost:8080/playdo-app";

var currentEmail;
var userSecret;
var role;

$(document).ready(function() {
	$("body").css("display", "none");
	
 	currentEmail = localStorage.getItem("user_email");
	userSecret = localStorage.getItem("user_secret");
	role = localStorage.getItem("user_role");
	
	checkAccess(currentEmail, currentEmail+role, userSecret);
	
	$("#logoffButton").bind("click", logoff);
	
});

var logoff = function() {
	console.log(currentEmail+ " " + userSecret);
	
	var hash = CryptoJS.HmacSHA256(currentEmail, userSecret);
	var hashInBase64 = CryptoJS.enc.Base64.stringify(hash);										
	var details = {	'hmac':hashInBase64,'email':currentEmail};
	
	var formBody = [];
		for (var property in details) {
			  var encodedKey = encodeURIComponent(property);
			  var encodedValue = encodeURIComponent(details[property]);
			  formBody.push(encodedKey + "=" + encodedValue);
		};	  
		formBody = formBody.join("&");
		
		$.ajax({
		     type: "post",
		     url: rootURL2 + "/rest/logoff",
		     data: formBody,
		     contentType: "application/x-www-form-urlencoded",
		     success: function(responseData, textStatus, jqXHR) {
		    	 console.log("------------- token deleted ------------");	
		    	 localStorage.setItem("user_email","");
		         localStorage.setItem("user_role","");
			     localStorage.setItem("user_secret","");	

			     swal({
			     	title : "Success",
			     	text : "You have successfully logged out.",
			     	icon : "success",
			    	buttons : false,
			     })
			     		
			     setTimeout( function() {
			     	redirect: window.location.replace(rootURL2+"/index.html");
			     }, 2000);
		     },
		     error: function(jqXHR, textStatus, errorThrown) {
		         		console.log("--------- error while deleting token ------------"+errorThrown);
		     }
		 });
		
}

function checkAccess(email, message, usersecret) {
	console.log("------------ check access, user's secret key is " + usersecret);
	  var hash = CryptoJS.HmacSHA256(message, usersecret);
	  var hashInBase64 = CryptoJS.enc.Base64.stringify(hash);	
	$.ajax({					
		type : 'GET',
		headers: {
		        'hmac':hashInBase64,
		        'email':email		       
		    },
		url : rootURL2 + "/rest/login/" + role,
		dataType: "text",
		success : function(data) {
			console.log("--------- checkAccess.js " + data + ", client hmac is " + hashInBase64);
			if(data == "accessdenied"){
				swal({
					title : "Error",
					text : "Access Denied",
					icon : "error",
					buttons : false,
				})
				setTimeout(
						function() { redirect: window.location.replace(rootURL2+"/index.html");	}, 200);
				 		
			}else{
				$("body").css("display", "block");	
			}
		}
	});				
};