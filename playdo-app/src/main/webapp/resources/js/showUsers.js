var rootURL = "http://localhost:8080/playdo-app";
var currentEmail = localStorage.getItem("user_email");
var currentCompanyId = localStorage.getItem("user_companyId");
var currentUserId = localStorage.getItem("user_userId");
var receiverId;

function findLoggedInUserByEmail(currentEmail) {
	console.log('findLoggedInUserByEmail: ' + currentEmail);
	$.ajax({
		type : 'GET',
		url : rootURL + "/rest/users/loggedin/"+currentEmail,
		dataType : "json",
		success : function(data) {
			$('.userName').text(data.firstName);
			$('#avatar').attr('src' + "'" + data.avatar+ "'");
			$('#name').text("Name: " + data.firstName + " " + data.lastName);
			$('#email').text("Email: " + currentEmail);
			$('#dob').text("DOB: " + data.dob);
			$('#team').text("Team: " + data.team.teamName);
			$('#points').text("Points: " + data.pointsEarned);
		}
	});
};

//Data for the cards
var renderUserCards = function(data) {
	output = '<div class="row">';
	$('#memberList').empty();
	$.each(data, function(index, user) {
		findAllUserSkillByUserId(user.userId);
		if(user.company.companyId == currentCompanyId && user.role == "user" && user.userId!= currentUserId){
		output += (
				'<div class="col-sm-6 col-md-4 col-lg-3">'	
					+'<div class="card">'
						+'<img class="userImagesOnCards" src="' + user.avatar + '"/><br/>'
						+ user.firstName + " " + user.lastName + '</p>' 
						+ user.email + '</p>'
						+ '<ul id="ul'+user.userId+'"></ul><br />'
						+ '<input class="btnContact" id="'+user.userId+'" data-toggle="modal" data-target="#contactModal" type="button" class="btn btn-warning " value="Contact" />'
					+ '</div>'
				+'</div>');
		}
	});
	output += '</div>';
	$('#memberList').append(output);
	
};

var findAllUserSkillByUserId = function(id) {
	console.log('findAllUserSkillByUserId function: ' + id);
	$.ajax({
		type : 'GET',
		url : rootURL + "/rest/userskills/user/" + id,
		dataType : "json",
		success : renderUserSkillListForEachUser
	});
};


function renderUserSkillListForEachUser(data){
	console.log('render user_skill list for each user');
	
	$.each(data, function(index, userskills){
		$('#ul'+userskills.user.userId).append(
			'<li id="'+ userskills.id +'">'+ userskills.skill.skillName +'</li>' 
		);
	});
};


var findAllUsersForCards = function() {
	console.log('findAllUsersforCards');
	$.ajax({
		type : 'GET',
		url : rootURL +  "/rest/users",
		dataType : "json",
		success : renderUserCards
	});
};


//**************AJAX CALLS RELATED TO teamCards************************

/*//Data for the cards
var renderTeamCards = function(data) {
	output = '<div class="row">';
	$.each(data, function(index, team) {
		output += (
				'<div class="col-sm-6 col-md-4 col-lg-3">'	
					+'<div class="card">' 
						+'<p><b>Name: </b>' + team.teamName + '</p>' 
						+'<p><b>email:</b> ' + team.project.projectName + '</p>' 
						+ '<input id="btnContact" data-toggle="modal" data-target="#contactModal" type="button" class="btn btn-warning " value="Contact" />'
					+ '</div>'
				+'</div>');
	});
	output += '</div>';
	$('#memberList').append(output);
};

var findAllTeamsForCards = function() {
	console.log('findAllTeamsforCards');
	$.ajax({
		type : 'GET',
		url : rootURL +  "/rest/team",
		dataType : "json",
		success : renderTeamCards
	});
};

var findByID = function(id) {
	console.log('findByID: ' + id);
	$.ajax({
		type : 'GET',
		url : rootURL + '/' + id,
		dataType : "json",
		success : function(data) {
			console.log('findByID success: ' + data.firstName);
			currentUser = data;
		}
	});
	
};*/


//**************AJAX CALLS RELATED TO UserSkills************************


var findAllUserSkill = function(){
	console.log('find all userskill function');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/userskills",
		dataType: "json",
		success: renderUserSkillList
	});
};

function renderUserSkillList(data){
	console.log('render user_skill list to table');
	
	$('#mySkills').empty();
	$.each(data, function(index, userskills){
		if(userskills.user.userId == currentUserId){
			$('#mySkills').append(
				'<li id="'+ userskills.id +'"><a href="#" id="'+ userskills.id +'"  class="btnUserSkillDelete">  <i class="fa fa-trash-o"></i></a>  '+ userskills.skill.skillName +' </li>'
			);
		}
	});
};


var findUserSkillById = function(id){
	console.log('find userskill by id  function');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/userskills/"+id,
		dataType: "json",
		success: function(data, textStatus, jqXHR) {
			return data.id;
		}
	});
};

var addUserSkill = function() {
	console.log('add userSkill function');
	
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootURL + '/rest/userskills',
		dataType : "json",
		data : formToJSONUserSkill(),
		success : function(data, textStatus, jqXHR) {
			 swal({
					title : "Success",
					text : " Skill saved successfully",
					icon : "success",
					button: "OK",
				});
			 findAllUserSkill();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to save skill",
					icon : "error",
					button:"OK",
				});
		}
	});
};

var formToJSONUserSkill = function() {
	return JSON.stringify({
	 "id": "",
        "user": {
            "userId": currentUserId,
            "email": "",
            "password": "",
            "salt": "",
            "firstName": "",
            "lastName": "",
            "dob": "",
            "pointsEarned": "",
            "role": "",
            "avatar": "",
            "token": "",
            "team": {
                "teamId": "",
                "teamName": "",
                "pointsEarned": "",
                "project": {
                    "projectId": "",
                    "projectName": "",
                    "startDate": "",
                    "endDate": "",
                    "company": {
                        "companyId":"",
                        "companyName": ""
                    }
                }
            },
            "company": {
                "companyId":"",
                "companyName": ""
            }
        },
        "skill": {
            "skillId": $('#selectSkillName').children(":selected").attr("id"),
            "skillName":  $('#selectSkillName').children(":selected").val(),
            "companyId": currentCompanyId
        }
	});
};


var deleteUserSkill = function(id) {
	console.log('delte userSkill function');
	$.ajax({
		type : 'DELETE',
		url : rootURL + "/rest/userskills/" + id,
		success : function(data, textStatus, jqXHR) {
			findAllUserSkill();
			 swal({
					title : "Success",
					text : " Skill deleted successfully",
					icon : "success",
					button : "OK",
				})
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to delete skill",
					icon : "error",
					buttons : "OK",
				})
		}
		
	});
};




//**************AJAX CALLS RELATED TO Skills************************


var findAllSkills = function(){
	console.log('find all skills function');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/skill",
		dataType: "json",
		success: renderSkillList
	});
};

function renderSkillList(data){
	console.log('Render skill list to dropdown');
	$.each(data, function(index, skill){
		if(skill.companyId == currentCompanyId){
			var x = document.getElementById("selectSkillName");
			var option = document.createElement("option");
			option.id = skill.skillId;
			option.text = skill.skillName;
			x.add(option);
		}
	});
};

var deleteSkill = function(id) {
	/*console.log('delte message function');
	$.ajax({
		type : 'DELETE',
		url : rootURL + "/rest/company/" + id,
		success : function(data, textStatus, jqXHR) {
			swal({
				title : "Success",
				text : " Company deleted successfully",
				icon : "success",
				button : "OK",
			})
			findAllMessages()
			
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to delete company",
					icon : "error",
					buttons : "OK",
				})
		}
	});*/
};


$(document).ready(function(){
	
	findLoggedInUserByEmail(currentEmail);
		
	findAllUsersForCards();
	findAllSkills();
	findAllUserSkill();
	
	$(document).on("click", '.btnUserSkillDelete', function(){
		deleteUserSkill(this.id);
		findAllUsersForCards();
	});
			
	$(document).on("click", '#btnAddSkill', function(){
		addUserSkill(this.id);
		findAllUsersForCards();
	});
	
	
});