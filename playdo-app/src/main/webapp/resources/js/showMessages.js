var rootURL = "http://localhost:8080/playdo-app";
var currentEmail = localStorage.getItem("user_email");
var currentCompanyId = localStorage.getItem("user_companyId");
var currentUserId = localStorage.getItem("user_userId");
var timeStamp;
var receiverId;
var receiverId2;
var currentSender;
var currentMessageId;
var currentMessageBoxId;


function findMessageById(id){
	$.ajax({
		type : 'GET',
		url : rootURL + '/rest/message/'+id,
		dataType : "json",
		success : function(data) {
			console.log('findByID success: ' + data.messageType);
			renderEmailToReadModal(data);
		}
	});
}
function renderEmailToReadModal(data){
	$('#fromRead').text("From: " + currentSender);
	$('#subjectRead').text(data.messageType);
	$('#dateRead').text(data.date);
	$('#messageTextRead').val("\n" + currentSender+" - wrote (on: "+ data.date + "):\n"+data.message);
}



var findAllMessagesInMessageBox = function(id){
	console.log('find all messages function for userId: ' + id);
	console.log(rootURL + "/rest/messages/search/" + id);
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/messages/search/" + id,
		dataType: "json",
		success: renderMessageList
	});
};
function renderMessageList(data){
	console.log('render message list to table');
	$('#messagesTable_body').empty();
	$.each(data, function(index, messagebox){
		var startTag ='';
		var endTag='';
		if(messagebox.new == true){
			startTag = '<strong>';
			endTag = '</strong>';
		} 
		$('#messagesTable_body').append(
			'<tr id="'+ messagebox.messageBoxId +'">'+
				'<td>'+startTag+'<a class="btnReadMessage" recId="'+messagebox.sender.userId+'" boxId="'+messagebox.messageBoxId+'" title="'+messagebox.sender.firstName + ' ' + messagebox.sender.lastName+'" id="'+messagebox.message.messageId+'" data-toggle="modal" data-target="#readMessageModal"> <i class="fa fa-envelope-o"></i></a>'+endTag+'</td>' +
				'<td>'+startTag + messagebox.message.date + endTag+'</td>' +
				'<td>'+startTag + messagebox.sender.email + endTag+'</td>' +
				'<td>'+startTag + messagebox.sender.firstName + " " + messagebox.sender.lastName   + endTag+'</td>' +
				'<td>'+startTag + messagebox.message.messageType + endTag+'</td>' +
				'<td>'+startTag + messagebox.closedBySender+ endTag+'</td>' +
				'<td>'+startTag+'<a class="btnCloseMessage" id="'+messagebox.message.messageId+'"><i class="fa fa-pencil-square-o"></i> '+ messagebox.closedByReceiver +'</a>'+endTag+'</td>' +
			'</tr>');
	});
	$('#messagesTable').DataTable();
};

function deleteMessageList(data){
	console.log('delete old message list');
	$.each(data, function(index, messagebox){
		if(messagebox.closedBySender == true && messagebox.closedByReceiver == true ){
			deleteMessage(messagebox.message.messageId);
		}
	});
};

var deleteMessage = function(id) {
	console.log('delte message function');
	$.ajax({
		type : 'DELETE',
		url : rootURL + "/rest/message/" + id,
		success : function(data, textStatus, jqXHR) {
			console.log("message deleted successfully");
			findAllMessagesInMessageBox();
			
		},
		error : function(jqXHR, textStatus, errorThrown) {
			console.log("failed to delete message");
		}
	});
};


var MessageToJSON = function(){
	timeStamp= new Date().getTime();
	return JSON.stringify({
		"messageId":currentUserId+timeStamp,
	    "date": new Date().getHours() + ":" + new Date().getMinutes() +" | "+ new Date().getDate() + "/" + new Date().getMonth() + "/" + new Date().getFullYear(),
	    "message": $("#messageText").val(),
	    "messageType": $('#selectSubject').children(":selected").val(),
	});
};

var saveMessage = function(){
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootURL + '/rest/message',
		dataType : "json",
		data : MessageToJSON(),
		success : function(data, textStatus, jqXHR) {
			 saveMessageBox();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to save message 1",
					icon : "error",
					button:"OK",
				});
		}
	});
	
};

var saveMessageBox = function(){
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootURL + '/rest/messages',
		dataType : "json",
		data : MessageBoxToJSON(),
		success : function(data, textStatus, jqXHR) {
			 swal({
					title : "Success",
					text : " Message sent",
					icon : "success",
					button: "OK",
				});
			 findAllMessagesInMessageBox(currentUserId);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to send message",
					icon : "error",
					button:"OK",
				});
		}
	});
	
};

var MessageBoxToJSON = function(){
	return JSON.stringify({
	     "closedBySender": false,
	     "closedByReceiver": false,
	     "receiverId": receiverId,
	     "sender": {
	         "userId": currentUserId,
	         "email": "",
	         "password": "",
	         "salt": "",
	         "firstName": "",
	         "lastName": "",
	         "dob": "",
	         "pointsEarned": "",
	         "role": "",
	         "avatar": "",
	         "token": "",
	         "team": {
	             "teamId": "",
	             "teamName": "",
	             "pointsEarned": "",
	             "project": {
	                 "projectId": "",
	                 "projectName": "",
	                 "startDate": "",
	                 "endDate": "",
	                 "company": {
	                     "companyId": "",
	                     "companyName": ""
	                 }
	             }
	         },
	         "company": {
	             "companyId": "" ,
	             "companyName": ""
	         }
	     },
	     "message": {
	         "messageId":parseInt(currentUserId+timeStamp),
	         "date": "",
	         "message": "",
	         "messageType": ""
	     },
	     "new": true
	});
};


var sendAnswerToJSON = function(){
	return JSON.stringify({
		"messageId":currentMessageId,
	    "date": new Date().getHours() + ":" +new Date().getMinutes() +" | "+ new Date().getDate() + "/" + new Date().getMonth() + "/" + new Date().getFullYear(),
	    "message": $("#messageTextRead").val(),
	    "messageType": $("#subjectRead").val(),
	});
}

var sendAnswer = function() {
	console.log('send answer');
	$.ajax({
		type : 'PUT',
		contentType : 'application/json',
		url : rootURL + '/rest/message/',
		dataType : "json",
		data : sendAnswerToJSON(),
		success : function(data, textStatus, jqXHR) {
			 swal({
					title : "Success",
					text : " Answer sent",
					icon : "success",
					button: "OK",
				});
			 setMessageToOld();
			 findAllMessagesInMessageBox(currentUserId);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to send answer",
					icon : "error",
					button:"OK",
				});
		}
	});
};


var setMessageToOld = function(){
	console.log('send answer');
	$.ajax({
		type : 'PUT',
		contentType : 'application/json',
		url : rootURL + '/rest/messages/',
		dataType : "json",
		data : setMessageToOldToJSON(),
		success : function(data, textStatus, jqXHR) {
			 console.log("success to set message to old")
			 findAllMessagesInMessageBox(currentUserId);
		},
		error : function(jqXHR, textStatus, errorThrown) {
			console.log("error to set to old");
		}
	});
}

var setMessageToOldToJSON = function(){
	return JSON.stringify({
		"messageBoxId": currentMessageBoxId ,
		 "closedBySender": "",
	     "closedByReceiver": "",
	     "receiverId": receiverId2,
	     "sender": {
	         "userId":currentUserId,
	         "email": "",
	         "password": "",
	         "salt": "",
	         "firstName": "",
	         "lastName": "",
	         "dob": "",
	         "pointsEarned": "",
	         "role": "",
	         "avatar": "",
	         "token": "",
	         "team": {
	             "teamId": "",
	             "teamName": "",
	             "pointsEarned": "",
	             "project": {
	                 "projectId": "",
	                 "projectName": "",
	                 "startDate": "",
	                 "endDate": "",
	                 "company": {
	                     "companyId": "",
	                     "companyName": ""
	                 }
	             }
	         },
	         "company": {
	             "companyId": "" ,
	             "companyName": ""
	         }
	     },
	     "message": {
	         "messageId":currentMessageId,
	         "date": "",
	         "message": "",
	         "messageType": ""
	     },
	     "new": true
	});

}

$(document).ready(function(){
	
	//findLoggedInUserByEmail(currentEmail);
	findAllMessagesInMessageBox(currentUserId);
	
	$(document).on("click", '#btnContactMember', function(){
		saveMessage();
	});
	
	$(document).on("click",".btnContact",function(){
		 receiverId = this.id;
	});
	
	$(document).on("click",".btnReadMessage",function(){
		currentMessageId = this.id;
		currentMessageBoxId = $(this).attr('boxId');
		receiverId2 = $(this).attr('recId');
		currentSender = $(this).attr("title");
		console.log("********************************" + currentMessageBoxId);
		findMessageById(currentMessageId);
		findAllMessagesInMessageBox(currentUserId);
	
	});
	
	$(document).on("click","#btnAnswerMessage",function(){
		sendAnswer(this.id);
	});
	
	
});
