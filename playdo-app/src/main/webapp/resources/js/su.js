/*var currentEmail = localStorage.getItem("user_email");
var userSecret = localStorage.getItem("user_secret");
var role = localStorage.getItem("user_role");*/

var rootURL = "http://localhost:8080/playdo-app";
var currentUser;
var loggedInUser;

//**************AJAX CALLS RELATED TO USERS ************************

function findLoggedInUserByEmail(currentEmail) {
	console.log('findLoggedInUserByEmail: ' + currentEmail);
	$.ajax({
		type : 'GET',
		url : rootURL + "/rest/users/loggedin/"+currentEmail,
		dataType : "json",
		success : function(data) {
			loggedInUser = data;
			welcome = data.firstName;
			$('.userName').text(data.firstName);
		}
	});
};

var findUserById = function(id) {
	console.log('findUserById function: ' + id);
	$.ajax({
		type : 'GET',
		url : rootURL + "/rest/users/" + id,
		dataType : "json",
		success : function(data) {
			console.log('findUserById success: ' + data.id);
			currentUser = data;
			renderDetails(currentUser);
		}
	});
};

var findAllUsers = function(){
	console.log('find all users function');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/users",
		dataType: "json",
		success: renderUserList
	});
};

function renderUserList(data){
	console.log("render user list to table");
	$('#table-body').empty();
	$.each(data, function(index, user){
		if(user.role == "admin" || user.role == "superuser" ){ 
			$('#table-body').append(
					'<tr id="'+ user.userId +'">'+
					'<td>'+ user.email +'</td>' +
					'<td>'+ user.firstName + ' ' + user.lastName +'</td>' +
					'<td>'+ user.dob +'</td>' +
					'<td>'+ user.role +'</td>' +
					'<td>'+ user.company.companyName +'</td>' +
					'<td>'+ '<a href="#" id="'+ user.userId +'" class="btnEditUser"><i class="fa fa-pencil-square-o"></i></a></td>'+	
					'<td>'+ '<a href="#" id="'+ user.userId +'" class="btnDelete"><i class="fa fa-trash-o"></i></a></td>'+			
			'</tr>');
		}
	});
	$('#table_id').DataTable();
};


var addUser = function() {
	console.log('add user function');
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootURL + '/rest/users',
		dataType : "json",
		data : formToJSONSave(),
		success : function(data, textStatus, jqXHR) {
			swal({
				title : "Success",
				text : " User saved successfully",
				icon : "success",
				button: "OK",
			});
			findAllUsers();
			resetFormSave();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			swal({
				title : "Error",
				text : "Failed to save user, please fill in all fields",
				icon : "error",
				button:"OK",
			});
			resetFormSave();
		}
	});
};


var formToJSONSave = function() {
	return JSON.stringify({
		    "userId": "",
		    "email": $('#emailSave').val(),
		    "password": "password12",
		    "salt": "",
		    "firstName":$('#firstNameSave').val(),
		    "lastName": $('#lastNameSave').val(),
		    "dob":  $('#dobSave').val(),
		    "pointsEarned": 0,
		    "role": $('#selectRole').val(),
		    "avatar": "resources/images/avatar/user.jpg",
		    "team": null,
		    "company": {
		        "companyId": $('#selectCompanyName').children(":selected").attr("id"),
		        "companyName": $('#selectCompanyName').children(":selected").val()
		}
	});
};


var deleteUser = function(id) {
	console.log('delte user function');
	$.ajax({
		type : 'DELETE',
		url : rootURL + "/rest/users/" + id,
		success : function(data, textStatus, jqXHR) {
			findAllUsers();
			 swal({
					title : "Success",
					text : " User deleted successfully",
					icon : "success",
					button : "OK",
				})
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to delete user",
					icon : "error",
					buttons : "OK",
				})
		}
	});
};


var resetFormSave = function() {
	$('#emailSave').val("");
	$('#firstNameSave').val("");
	$('#lastNameSave').val("");
	$('#dobSave').val("");
}


//**************AJAX CALLS RELATED TO COMPANY************************



var findAllCompany = function(){
	console.log('find all company function');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/company",
		dataType: "json",
		success: renderCompanyList
	});
};

function renderCompanyList(data){
	console.log('render company list to table');
	$('#companyTable_body').empty();
	$.each(data, function(index, company){
		$('#companyTable_body').append(
			'<tr id="'+ company.companyId +'">'+
				'<td>'+ company.companyName +'</td>' +
				'<td>'+ '<a href="#" id="'+ company.companyId +'" class="btnEditCompany"><i class="fa fa-pencil-square-o"></i></a></td>'+
				'<td>'+ '<a href="#" id="'+ company.companyId +'" class="btnDeleteCompany"><i class="fa fa-trash-o"></i></a></td>'+			
			'</tr>');
		});
	$('#companyTable_id').DataTable();
	
	console.log('Render company list to dropdown');
	resetCompanyList(data);
	$.each(data, function(index, company){
			var x = document.getElementById("selectCompanyName");
			var option = document.createElement("option");
			option.id = company.companyId;
			option.text = company.companyName;
			x.add(option);
	});
};

function resetCompanyList(data){
	console.log('Reset company list dropdown');
	$.each(data, function(index, company){
			var x = document.getElementById("selectCompanyName");
			var option = document.createElement("option");
			option.id = company.companyId;
			option.text = company.companyName;
			x.remove(option);
	});
};

var addCompany = function() {
	console.log('add company function');
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootURL+ "/rest/company" ,
		dataType : "json",
		data : formToJSONSaveCompany(),
		success : function(data, textStatus, jqXHR) {
			 swal({
					title : "Success",
					text : "Company saved successfully",
					icon : "success",
					button : "OK",
			})
			findAllCompany();
			resetAddCompanyForm();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			swal({
				title : "Error",
				text : "Failed to save company",
				icon : "error",
				button : "OK",
			})
			resetAddCompanyForm();
		}
	});
	$('#addCompanyModal').close();
};

var deleteCompany = function(id) {
	console.log('delte company function');
	$.ajax({
		type : 'DELETE',
		url : rootURL + "/rest/company/" + id,
		success : function(data, textStatus, jqXHR) {
			swal({
				title : "Success",
				text : " Company deleted successfully",
				icon : "success",
				button : "OK",
			})
			findAllCompany()
			findAllCompanyForDropDown()
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to delete company",
					icon : "error",
					buttons : "OK",
				})
		}
	});
};

var formToJSONSaveCompany = function() {
	return JSON.stringify({
			"companyId":"",
			"companyName" : $('#companyNameSave').val(),
	});
};

var resetAddCompanyForm = function() {
	$('#companyNameSave').val("");
}

//************** VALIDATE ************************

var validateEmail = function(email) {
	  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	  return re.test(email);
	}

var validate = function() {
	var email = $("#emailSave").val();
	if (validateEmail(email)) {
		addUser();
	} else {
		swal({
			title : "Error",
			text : "email is not in valid format",
			icon : "error",
			button : "OK",	
		})
	  }
		resetFormSave();
	  return false;
}

var validateCompany = function() {
	var company = $("#companyNameSave").val();
	if (company != null && company !="") {
		addCompany();
	} else {
		swal({
			title : "Error",
			text : "please enter company name",
			icon : "error",
			button : "OK",	
		})
	  }
		resetAddCompanyForm();
	  return false;
}
	



//**************PIE CHART************************

/*google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawChart);

// Draw the chart and set the chart values
function drawChart() {
  var data = google.visualization.arrayToDataTable([
  ['Company', 'Admin'],
  ['PlayDo', 1],
  ['First Company', 2],
  ['Second Company', 2],
  ['Third Company', 0],
  ['Fourth Company', 0],
]);

  // Optional; add a title and set the width and height of the chart
  var options = {'title':'Admins registered per company', 'width':800, 'height':600};

  // Display the chart inside the <div> element with id="piechart"
  var chart = new google.visualization.PieChart(document.getElementById('piechart'));
  chart.draw(data, options);
}*/


//**************DOCUMENT READY************************



$(document).ready(function(){
	currentEmail = localStorage.getItem("user_email");
	findLoggedInUserByEmail(currentEmail);
	
	$("#dobSave").flatpickr({
		enableTime: true,
		dateFormat: "d/m/Y"
	});
	
	resetAddCompanyForm();
	resetFormSave();
	findAllUsers();
	findAllCompany();
	
	$("#btnSave").bind("click", validate);
	
	$("#btnSaveCompany").bind("click", validateCompany);
	
	$(document).on("click", '.btnDelete', function(){
		deleteUser(this.id);
	});
	
	$(document).on("click", '.btnDeleteCompany', function(){
		deleteCompany(this.id);
	});
	
});


















