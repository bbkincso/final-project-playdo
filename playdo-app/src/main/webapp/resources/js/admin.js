var rootURL = "http://localhost:8080/playdo-app";
var loggedInUser;
var projectStart =  $("#projectStartSave").val();

//**************AJAX CALLS RELATED TO USERS ************************


function findLoggedInUserByEmail(currentEmail) {
	console.log('findLoggedInUserByEmail: ' + currentEmail);
	$.ajax({
		type : 'GET',
		url : rootURL + "/rest/users/loggedin/"+currentEmail,
		dataType : "json",
		success : function(data) {
			loggedInUser = data;
			welcome = data.firstName;
			$('.userName').text(data.firstName);
		}
	});
};

var findAllUser = function(){
	console.log('find all user function');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/users",
		dataType: "json",
		success: renderUserList
	});
};

function renderUserList(data){
	console.log("Render User List");
	
	$('#userTable_body').empty();
	$('#adminTable_body').empty();
	
	$.each(data, function(index, user){
		if(user.role == "user" && user.company.companyId == loggedInUser.company.companyId){ 
				$('#userTable_body').append(
					'<tr>'+
						'<td><img src="' + user.avatar + '"/></td>'+
						'<td>'+ user.firstName + ' '+ user.lastName + '</td>'+
						'<td>'+ user.dob +'</td>' +
						'<td>'+ user.pointsEarned +'</td>' +
						'<td>'+ user.team.teamName +'</td>' +
						'<td>'+ user.team.project.projectName + '</td>'+
						'<td>'+ user.email + '</td>'+
						'<td>'+ '<a href="#" id="'+ user.userId +'" class="btnEditUser"><i class="fa fa-pencil-square-o"></i></a></td>'+	
						'<td>'+ '<a href="#" id="'+ user.userId +'" class="userBtnDelete"><i class="fa fa-trash-o"></i></a></td>'+
					'</tr>'
				);
			} 
			if(user.role == "admin" && user.company.companyId == loggedInUser.company.companyId){ 
				$('#adminTable_body').append(
					'<tr>'+
						'<td>'+ user.firstName + ' '+ user.lastName + '</td>'+
						'<td>'+ user.dob + '</td>'+
						'<td>'+ user.role + '</td>'+
						'<td>'+ user.email + '</td>'+
						'<td>'+ '<a href="#" id="'+ user.userId +'" class="btnEditAdmin"><i class="fa fa-pencil-square-o"></i></a></td>'+	
						'<td>'+ '<a href="#" id="'+ user.userId +'" class="adminBtnDelete"><i class="fa fa-trash-o"></i></a></td>'+
					'</tr>'
				);
			} 
	});
	
	$('#userTable_id').DataTable();
	$('#adminTable_id').DataTable();
};


var addUser = function() {
	console.log('add user function');
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootURL + '/rest/users',
		dataType : "json",
		data : userToJSONSave(),
		success : function(data, textStatus, jqXHR) {
			 swal({
					title : "Success",
					text : "User saved successfully",
					icon : "success",
					button: "OK",
				});
			 findAllUser();
			 resetUserForm();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			swal({
				title : "Error",
				text : "Failed to save user, please fill in all fields",
				icon : "error",
				button:"OK",
			});
		}
	});
};


var userToJSONSave = function() {
	return JSON.stringify({
		"userId":"",
		"email" : $('#userEmailSave').val(),
		"password": "password12",
	    "salt": "",
		"firstName" : $('#userFirstNameSave').val(),
		"lastName" : $('#userLastNameSave').val(),
		"dob" : $('#userDobSave').val(),
		"pointsEarned" : 0,
		"role" :$('#userSelectRole').val(),
		"avatar": "resources/images/avatar/user.jpg",
		"team": {
			"teamId": $('#userSelectTeam').children(":selected").attr("id"),
			"teamName": $('#userSelectTeam').children(":selected").val(),
			"pointsEarned": "",
            "project": {
                "projectId": "",
                "projectName": "",
                "startDate": "",
                "endDate": "",
                "company": {
                    "companyId": "",
                    "companyName": ""
                }
            }
		},
		"company": {
	        "companyId": loggedInUser.company.companyId,
	        "companyName": loggedInUser.company.companyName
		}
	});
};

var deleteUser = function(id) {
	console.log('delteUser');
	$.ajax({
		type : 'DELETE',
		url : rootURL + "/rest/users/" + id,
		success : function(data, textStatus, jqXHR) {
			findAllUser();
			 swal({
					title : "Success",
					text : " User deleted successfully",
					icon : "success",
					button : "OK",
				})
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to delete user",
					icon : "error",
					buttons : "OK",
				})
		}
	});
};


var resetUserForm = function() {
	$('#userEmailSave').val("");
	$('#userFirstNameSave').val("");
	$('#userLastNameSave').val("");
	$('#userDobSave').val("");
	$('#userSelectRole').val("");
}

//**************AJAX CALLS RELATED TO TEAMS ************************

var findAllTeam = function(){
	console.log('findAllTeam');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/team",
		dataType: "json",
		success: renderTeamList
	});
};

function renderTeamList(data){
	console.log("render team list");
	$('#teamTable_body').empty();
	$.each(data, function(index, team){
			if(team.project.company.companyId == loggedInUser.company.companyId){
				$('#teamTable_body').append(
					'<tr id="'+ team.teamId +'">'+
						'<td>' + team.teamName + '</td>'+
						'<td>' + team.pointsEarned + '</td>'+
						'<td>' + team.project.projectName + '</td>'+
						'<td>' + team.project.company.companyName+ '</td>'+
						'<td>'+ '<a href="#" id="'+ team.teamId +'" class="btnEditTeam"><i class="fa fa-pencil-square-o"></i></a></td>'+	
						'<td>'+ '<a href="#" id="'+ team.teamId +'" class="teamBtnDelete"><i class="fa fa-trash-o"></i></a></td>'+
					'</tr>');
		}
	});
	$('#teamTable_id').DataTable();
	
	console.log('Render team list to dropdown');
	resetTeamListToDropDown(data);
	$.each(data, function(index, team){
			var x = document.getElementById("userSelectTeam");
			var option = document.createElement("option");
			if ( team.project.company.companyId == loggedInUser.company.companyId ){
				option.id = team.teamId;
				option.text = team.teamName;
				x.add(option);
			}
	});
	
};

function resetTeamListToDropDown(data){
	console.log('Reset team list dropdown');
	$.each(data, function(index, team){
			var x = document.getElementById("userSelectTeam");
			var option = document.createElement("option");
			x.remove(option);
	});
};

var addTeam = function() {
	console.log('add team function');
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootURL + '/rest/team',
		dataType : "json",
		data : formToJSONTeam(),
		success : function(data, textStatus, jqXHR) {
			 swal({
					title : "Success",
					text : " Team saved successfully",
					icon : "success",
					button: "OK",
				});
			 findAllTeam();
			 resetTeamForm();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to save project, please fill in all fields",
					icon : "error",
					button:"OK",
				});
		}
	});
};

var formToJSONTeam = function() {
	return JSON.stringify({
		 "teamId": "",
	        "teamName": $("#teamNameSave").val(),
	        "pointsEarned": 0,
	        "project": {
	            "projectId": $('#teamSelectProject').children(":selected").attr("id"), 
	            "projectName": $('#teamSelectProject').children(":selected").val(),
	            "startDate": "",
	            "endDate": "",
	            "company": {
	                "companyId": "",
	                "companyName": ""
	            }
	        }
	});
};

var deleteTeam = function(id) {
	console.log('delte team function');
	$.ajax({
		type : 'DELETE',
		url : rootURL + "/rest/team/" + id,
		success : function(data, textStatus, jqXHR) {
			findAllTeam();
			 swal({
					title : "Success",
					text : " Team deleted successfully",
					icon : "success",
					button : "OK",
				})
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to delete team",
					icon : "error",
					buttons : "OK",
				})
		}
	});
};

var resetTeamForm = function() {
	$("#teamNameSave").val("");
}



//**************AJAX CALLS RELATED TO PROJECTS************************

var findAllProject = function(){
	console.log('findAllProject');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/project",
		dataType: "json",
		success: renderProjectList
	});
};

function renderProjectList(data){
	$('#projectTable_body').empty();
	$.each(data, function(index, project){
		if(project.company.companyId == loggedInUser.company.companyId){
			$('#projectTable_body').append('<tr id="'+project.projectId +'">'+
					'<td>'+ project.projectName + '</td>'+
					'<td>'+ project.startDate + '</td>'+
					'<td>'+ project.endDate + '</td>'+
					'<td>'+ project.company.companyName + '</td>'+
					'<td>'+ '<a href="#" id="'+ project.projectId +'" class="btnEditProject"><i class="fa fa-pencil-square-o"></i></a></td>'+	
					'<td>'+ '<a href="#" id="'+ project.projectId +'" class="projectBtnDelete"><i class="fa fa-trash-o"></i></a></td>'+			
					'</tr>');
		}
	});
	$('#projectTable_id').DataTable();
	
	console.log('Render project list to dropdown');
	resetProjectListToDropDown(data);
	$.each(data, function(index, project){
			var x = document.getElementById("teamSelectProject");
			var option = document.createElement("option");
			if ( project.company.companyId == loggedInUser.company.companyId ){
				option.id = project.projectId;
				option.text = project.projectName;
				x.add(option);
			}
	});
	
};

function resetProjectListToDropDown(data){
	console.log('Reset project list dropdown');
	$.each(data, function(index, project){
			var x = document.getElementById("teamSelectProject");
			var option = document.createElement("option");
			x.remove(option);
	});
};

var addProject = function() {
	console.log('addProject');
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootURL + '/rest/project',
		dataType : "json",
		data : formToJSONProject(),
		success : function(data, textStatus, jqXHR) {
			 swal({
					title : "Success",
					text : " Project saved successfully",
					icon : "success",
					button: "OK",
				});
			 findAllProject();
			 resetProjectForm();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to save project, please fill in all fields",
					icon : "error",
					button:"OK",
				});
		}
	});
};

var formToJSONProject = function() {
	return JSON.stringify({
		"projectId":"",
		"projectName" : $('#projectNameSave').val(),
		"startDate" : $('#projectStartSave').val(),
		"endDate" : $('#projectEndSave').val(),
		"company" : {
			"companyId": loggedInUser.company.companyId,
			"companyName": loggedInUser.company.companyName
		}
	});
};

var deleteProject = function(id) {
	console.log('delteUser');
	$.ajax({
		type : 'DELETE',
		url : rootURL + "/rest/project/" + id,
		success : function(data, textStatus, jqXHR) {
			 swal({
					title : "Success",
					text : " Project deleted successfully",
					icon : "success",
					button : "OK",
				})
				findAllProject();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			 swal({
					title : "Error",
					text : "Failed to delete project",
					icon : "error",
					buttons : "OK",
				})
		}
	});
};


var resetProjectForm = function() {
	$('#projectNameSave').val("");
	$('#projectStartSave').val("");
	$('#projectEndSave').val("")
}

//************** VALIDATE ************************

var validateEmail = function(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

var validate = function() {
	var email = $("#userEmailSave").val();
	if (validateEmail(email)) {
		addUser();
	} else {
		swal({
			title : "Error",
			text : "email is not in valid format",
			icon : "error",
			button : "OK",	
		})
	  }
	  return false;
}


//**************DOCUMENT READY************************


 
$(document).ready(function(){
	currentEmail = localStorage.getItem("user_email");
	findLoggedInUserByEmail(currentEmail);
	
	resetProjectForm();
	resetTeamForm();
	resetUserForm();
	
	$("#userDobSave").flatpickr({
		enableTime: true,
		dateFormat: "d/m/Y",
		maxDate: "today"
	});
	
	$("#projectStartSave").flatpickr({
		enableTime: true,
		dateFormat: "d/m/Y",
		minDate: "today"
	});
	
	$("#projectEndSave").flatpickr({
		enableTime: true,
		dateFormat: "d/m/Y",
		minDate: "today"
	});
	
	findAllProject();
	findAllTeam();
	findAllUser();
	
	$("#userBtnSave").bind("click", validate);
	
	$(document).on("click", "#btnSaveTeam", function(){
		addTeam();
	});
	
	$(document).on("click", "#btnSaveProject", function(){
		addProject();
	});
	
	$(document).on("click", '.teamBtnDelete', function(){
		deleteTeam(this.id);
	});
	
	$(document).on("click", '.projectBtnDelete', function(){
		deleteProject(this.id);
	});
	
	$(document).on("click", '.adminBtnDelete', function(){
		deleteUser(this.id);
	});
	
	$(document).on("click", '.userBtnDelete', function(){
		deleteUser(this.id);
	});
	
});


















