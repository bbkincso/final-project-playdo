var rootURL = "http://localhost:8080/playdo-app";

var validateEmail = function(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

var validatePassword = function(password){
	var reg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/;
	return reg.test(password);
}

var validate = function() {	
	var password = $("#userPassword").val();
	var email = $("#userName").val();
	if (validateEmail(email) && validatePassword(password)) {
		login(); 
    } else {
		swal({
			title : "Error",
			text : "Password or email is not valid",
			icon : "error",
			buttons : false,
		});
	}
	return false;
}

var login = function() {
	console.log('login');
	var password = $("#userPassword").val();
	var email = $("#userName").val();
	var details = {	'email': email,  'password': password};
	var formBody = [];
	
	for (var property in details) {
		  var encodedKey = encodeURIComponent(property);
		  var encodedValue = encodeURIComponent(details[property]);
		  formBody.push(encodedKey + "=" + encodedValue);
	};	  
	formBody = formBody.join("&");

	$.ajax({
		type : 'POST',
		url : rootURL + "/rest/login",
		data: formBody,
		contentType : "application/x-www-form-urlencoded",
		success: function(responseData, textStatus, jqXHR) {
			var role = responseData["role"];
			var token = responseData["token"];
			var userId = responseData["userId"];
			var companyId =  responseData["companyId"];
			console.log("------------- main.js loginAJAX role: " + role + " token: " + token );
		
			if(role == "index"){					
				swal({
					title : "Error",
					text : "Password or email is not valid AJAX",
					icon : "error",
					buttons : false
				})
				setTimeout(	function() { 
					redirect: window.location.replace(rootURL + "/index.html");	
				}, 10000);						
			}	
			
			localStorage.setItem("user_userId",userId);
			localStorage.setItem("user_companyId",companyId);
	    	localStorage.setItem("user_email",email);
        	localStorage.setItem("user_role",role);
        	localStorage.setItem("user_secret",token);
        	
			if (role == "superuser"){
	        	console.log(role);
				redirect: window.location.replace(rootURL + "/superuser.html");
			}
			if (role == "admin"){
	        	console.log(role);
				redirect: window.location.replace(rootURL + "/admin.html");
			}
			if (role == "user"){
	        	console.log(role);
				redirect: window.location.replace(rootURL +  "/user.html");
			}
		},
	     error: function(jqXHR, textStatus, errorThrown) {
      		console.log(errorThrown);
	     }
	})
}

function close_window(currentURL, newURL){
    var newWindow = window.open(newURL, '_self', ''); // open the new	// window
    window.close(url); // close the current window
}

$(document).ready(function(){

	$("#loginButton").bind("click", validate);  
	
	$('form').on('submit', (event)=>{
	    event.preventDefault();
	    
	    if (event.keyCode === 13) {
	    	$("#loginButton").bind("click", validate); 
	    }
	});
	
});
