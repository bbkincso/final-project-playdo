$(document).ready(function() {
	$('#progress').css("display","none");
	
	$("#dropFiles").on('dragenter', function(ev) {
		$("#dropFiles").addClass("highlightDropArea");
	});
	
	$("#dropFiles").on('dragleave', function(ev) {
		$("#dropFiles").removeClass("highlightDropArea");
	});
	
	$("#dropFiles").on('drop', function(ev) {
		ev.preventDefault();
	    ev.stopPropagation();
	    
	    if(ev.originalEvent.dataTransfer){
	    	if(ev.originalEvent.dataTransfer.files.length) {
	    		
	    		var droppedFiles = ev.originalEvent.dataTransfer.files;
	    		console.log(droppedFiles.path);
	    		
	    		for (var i = 0; i < droppedFiles.length; i++){
	    			console.log(droppedFiles[i]);
	    			
	    			$("#confirmation").append("<br /> <b>Dropped File </b>"+ droppedFiles[i].name);
	    			$('#progress').css("display","block");
	    			//importFile(droppedFiles[i].name);
	    		}
	    	}
	    }
	  
	    $("#dropFiles").removeClass("highlightDropArea");
	    return false;
	});
	
	$("#dropFiles").on('dragover', function(ev) {
		ev.preventDefault();
	});
});

function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regexS = "[\\?&]" + name + "=([^&#]*)";
	var regex = new RegExp(regexS);
	var results = regex.exec(window.location.href);
	if (results == null)
		return "";
	else
		return decodeURIComponent(results[1].replace(/\+/g, " "));
}