/*var rootURL = "http://localhost:8080/playdo-app";

//**************AJAX CALLS RELATED TO USERS ************************


function findLoggedInUserByEmail(currentEmail) {
	console.log('findLoggedInUserByEmail: ' + currentEmail);
	$.ajax({
		type : 'GET',
		url : rootURL + "/rest/users/loggedin/"+currentEmail,
		dataType : "json",
		success : function(data) {
			console.log(data);
			$('.userName').text(data.firstName);
			$('#avatar').attr('src' + "'" + data.avatar+ "'");
			$('#name').text("Name: " + data.firstName + " " + data.lastName);
			$('#email').text("Email: " + currentEmail);
			$('#dob').text("DOB: " + data.dob);
			$('#team').text("Team: " + data.team.teamName);
			$('#points').text("Points: " + data.pointsEarned);
		}
	});
};

var findAllUser = function(){
	console.log('find all user function');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/users",
		dataType: "json",
		success: renderUserList
	});
};

function renderUserList(data){
	console.log("Render User List");
	
	$.each(data, function(index, user){
		if(user.company.companyId == loggedInUser.company.companyId){ 
				
		} 
	});
};


var addUser = function() {
	console.log('add user function');
	$.ajax({
		type : 'POST',
		contentType : 'application/json',
		url : rootURL + '/rest/users',
		dataType : "json",
		data : userToJSONSave(),
		success : function(data, textStatus, jqXHR) {
			 swal({
					title : "Success",
					text : "User saved successfully",
					icon : "success",
					button: "OK",
				});
			 findAllUser();
			 resetUserForm();
		},
		error : function(jqXHR, textStatus, errorThrown) {
			swal({
				title : "Error",
				text : "Failed to save user, please fill in all fields",
				icon : "error",
				button:"OK",
			});
		}
	});
};


var userToJSONSave = function() {
	return JSON.stringify({
		"userId":"",
		"email" : $('#userEmailSave').val(),
		"password": $('#userPasswordSave').val(),
	    "salt": "",
		"firstName" : $('#userFirstNameSave').val(),
		"lastName" : $('#userLastNameSave').val(),
		"dob" : $('#userDobSave').val(),
		"pointsEarned" : "",
		"role" :"",
		"avatar": "",
		"team": "",
		"company": ""
	});
};

var resetUserForm = function() {
	$('#userEmailSave').val("");
	$('#userFirstNameSave').val("");
	$('#userLastNameSave').val("");
	$('#userDobSave').val("");
	$('#userSelectRole').val("");
}

//**************AJAX CALLS RELATED TO TEAMS ************************

var findAllTeam = function(){
	console.log('findAllTeam');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/team",
		dataType: "json",
		success: renderTeamList
	});
};

//**************AJAX CALLS RELATED TO PROJECTS************************

var findAllProject = function(){
	console.log('findAllProject');
	$.ajax({
		type: 'GET',
		url: rootURL + "/rest/project",
		dataType: "json",
		success: renderProjectList
	});
};

//************** VALIDATE ************************

var validateEmail = function(email) {
	var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
}

var validate = function() {
	var email = $("#userEmailSave").val();
	if (validateEmail(email)) {
		addUser();
	} else {
		swal({
			title : "Error",
			text : "email is not in valid format",
			icon : "error",
			button : "OK",	
		})
	  }
	  return false;
}
//**************DOCUMENT READY************************


 
$(document).ready(function(){
	var currentEmail = localStorage.getItem("user_email");
	findLoggedInUserByEmail(currentEmail);
	resetUserForm();
	
	findAllProject();
	findAllTeam();
	findAllUser();
	
	$("#userBtnSave").bind("click", validate);
	
});


















*/