package com.playdo.playdo_app.controller;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.DatatypeConverter;

import org.apache.commons.codec.binary.Base64;

@Stateless
@LocalBean
public class Secure {

	public String hash(String password) {
		MessageDigest algorithm = null;
		try {
			algorithm = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		algorithm.reset();
		algorithm.update(password.getBytes());
		byte[] messageDigest = algorithm.digest();
		String encodedDigest = DatatypeConverter.printBase64Binary(messageDigest);
		return encodedDigest;
	}

	public String generateSalt() {
		SecureRandom random = new SecureRandom();
		byte bytes[] = new byte[20];
		random.nextBytes(bytes);
		String salt = DatatypeConverter.printBase64Binary(bytes);
		return salt;
	}
	
	public String generateToken() {
		KeyGenerator kg = null;			
		try {
			kg = KeyGenerator.getInstance("HmacSHA256");
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	    SecretKey sk = kg.generateKey();		   
	    String encodedKey = Base64.encodeBase64String( sk.getEncoded() );
	    encodedKey = encodedKey.replace("+","a");
	    
	    return encodedKey;
	}
	
    public static String getBase64EncodedHmac(String message, String secretKeyString) {
    	String hash = "";
    	try {    	     
    	     Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
    	     SecretKeySpec secret_key = new SecretKeySpec(secretKeyString.getBytes(), "HmacSHA256");
    	     sha256_HMAC.init(secret_key);
    	     hash = Base64.encodeBase64String(sha256_HMAC.doFinal(message.getBytes()));    	     
    	} catch (Exception e){
    	     System.out.println("Error");
    	}
    	return hash;    	
    }
    
}
