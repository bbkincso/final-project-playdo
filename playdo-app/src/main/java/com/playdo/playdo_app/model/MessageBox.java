package com.playdo.playdo_app.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class MessageBox {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int messageBoxId;
	private boolean closedBySender;
	private boolean closedByReceiver;
	private int receiverId;
	private boolean isNew;
	
	@ManyToOne
	@JoinColumn(name = "senderId")
	private Users sender;
	
	@ManyToOne
	@JoinColumn(name = "messageId")
	private Message message;

	public int getMessageBoxId() {
		return messageBoxId;
	}

	public void setMessageBoxId(int messageBoxId) {
		this.messageBoxId = messageBoxId;
	}

	public int getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(int receiverId) {
		this.receiverId = receiverId;
	}

	public Users getSender() {
		return sender;
	}

	public void setSender(Users sender) {
		this.sender = sender;
	}

	public Message getMessage() {
		return message;
	}

	public void setMessage(Message message) {
		this.message = message;
	}

	public boolean isClosedBySender() {
		return closedBySender;
	}

	public void setClosedBySender(boolean closedBySender) {
		this.closedBySender = closedBySender;
	}

	public boolean isClosedByReceiver() {
		return closedByReceiver;
	}

	public void setClosedByReceiver(boolean closedByReceiver) {
		this.closedByReceiver = closedByReceiver;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

}
