package com.playdo.playdo_app.model;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Message {

	@Id
	private BigInteger messageId;
	private String date;
	private String message;
	private String messageType;
	
	@OneToMany(/*fetch = FetchType.LAZY,*/ mappedBy = "message", cascade = CascadeType.ALL)
	private Set<MessageBox> messagesInBox = new HashSet<MessageBox>(0);
	
	
	public BigInteger getMessageId() {
		return messageId;
	}
	public void setMessageId(BigInteger messageId) {
		this.messageId = messageId;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessageType() {
		return messageType;
	}
	public void setMessageType(String messageType) {
		this.messageType = messageType;
	}
}
