package com.playdo.playdo_app.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int companyId;
	private String companyName;

	@OneToMany(/*fetch = FetchType.LAZY,*/ mappedBy = "company", cascade = CascadeType.ALL )
	private Set<Users> companyUsers = new HashSet<Users>(0);
	
	@OneToMany(/*fetch = FetchType.LAZY,*/ mappedBy = "company", cascade = CascadeType.ALL)
	private Set<Project> companyProjects = new HashSet<Project>(0);

	public int getCompanyId() {
		return companyId;
	}

	public void setCompanyId(int companyId) {
		this.companyId = companyId;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String name) {
		this.companyName = name;
	}

}
