package com.playdo.playdo_app.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.playdo.playdo_app.model.Team;

@Stateless
@LocalBean
public class TeamDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<Team> getAll() {
		Query query = em.createQuery("SELECT t FROM Team t");
		return query.getResultList();
	}

	public Team getById(int id) {
		return em.find(Team.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Team> getTeamByTeamName(String teamName) {
		final Query query = em.createQuery("SELECT t FROM Team AS t " + "WHERE t.teamName LIKE ?1");
		query.setParameter(1, "%" + teamName.toUpperCase() + "%");
		return query.getResultList();
	}
	
/*	@SuppressWarnings("unchecked")
	public List<Team> getTeamWithProjectName() {
		Query query = em.createQuery("SELECT DISTINCT t.teamId, t.teamName, t.pointsEarned, p.projectName, t.companyId, p.projectId, t.projectId"
								    +" FROM Team AS t, Project AS p WHERE t.projectId = p.projectId ");
		return query.getResultList();
	}*/
	
	public void save(Team team){
		em.persist(team);
	}

	public void update(Team team) {
		em.merge(team);
	}

	public void delete(int id) {
		em.remove(getById(id));
	}

	

}
