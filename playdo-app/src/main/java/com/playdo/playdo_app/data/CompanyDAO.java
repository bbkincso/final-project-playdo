package com.playdo.playdo_app.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.playdo.playdo_app.model.Company;

@Stateless
@LocalBean
public class CompanyDAO{

	@PersistenceContext
	private EntityManager em;

	@SuppressWarnings("unchecked")
	public List<Company> getAll() {
		Query query = em.createQuery("SELECT DISTINCT c FROM Company c");
		return query.getResultList();
	}

	public Company getById(int id) {
		return em.find(Company.class, id);
	}
	public void save(Company company) {
		em.persist(company);
	}
	public void update(Company company) {
		em.merge(company);
	}
	public void delete(int id) {
		em.remove(getById(id));
	}

}
