package com.playdo.playdo_app.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.playdo.playdo_app.model.Project;

@Stateless
@LocalBean
public class ProjectDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<Project> getAll() {
		Query query = em.createQuery("SELECT p FROM Project p");
		return query.getResultList();
	}

	public Project getById(int id) {
		return em.find(Project.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<Project> getProjectByProjectName(String projectName) {
		final Query query = em.createQuery("SELECT p FROM Project AS p " + "WHERE p.projectName LIKE ?1");
		query.setParameter(1, "%" + projectName.toUpperCase() + "%");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Project> getProjectByCompanyId(int companyId) {
		final Query query = em.createQuery("SELECT p FROM Project AS p " + "WHERE p.companyId LIKE ?1");
		query.setParameter(1, "%" + companyId + "%");
		return query.getResultList();
	}

	
	public void save(Project project){
		em.persist(project);
	}

	public void update(Project project) {
		em.merge(project);
	}

	public void delete(int id) {
		em.remove(getById(id));
	}
	

}
