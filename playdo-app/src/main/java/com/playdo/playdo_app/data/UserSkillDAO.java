package com.playdo.playdo_app.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.playdo.playdo_app.model.UserSkill;

@Stateless
@LocalBean
public class UserSkillDAO {
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<UserSkill> getAll() {
		Query query = em.createQuery("SELECT s FROM UserSkill s");
		return query.getResultList();
	}

	public UserSkill getById(int id) {
		return em.find(UserSkill.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<UserSkill> getSkillByUserId(int userId) {
		Query query = em.createQuery("SELECT s FROM UserSkill s WHERE s.user.userId = ?1");
		query.setParameter(1, userId);
		return query.getResultList();		
	}
	
	public void save(UserSkill skill){
		em.persist(skill);
	}

	public void delete(int id) {
		em.remove(getById(id));
	}

}
