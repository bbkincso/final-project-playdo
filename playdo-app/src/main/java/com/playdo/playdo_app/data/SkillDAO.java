package com.playdo.playdo_app.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.playdo.playdo_app.model.Skill;

@Stateless
@LocalBean
public class SkillDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<Skill> getAll() {
		Query query = em.createQuery("SELECT s FROM Skill s");
		return query.getResultList();
	}

	public Skill getById(int skillId) {
		return em.find(Skill.class, skillId);
	}

	@SuppressWarnings("unchecked")
	public List<Skill> getSkillBySkillName(String skillName) {
		final Query query = em.createQuery("SELECT s FROM Skill AS s " + "WHERE s.skillName LIKE ?1");
		query.setParameter(1, "%" + skillName.toUpperCase() + "%");
		return query.getResultList();
	}

	public void save(Skill skill){
		em.persist(skill);
	}

	public void update(Skill skill) {
		em.merge(skill);
	}

	public void delete(int skillId) {
		em.remove(getById(skillId));
	}


}
