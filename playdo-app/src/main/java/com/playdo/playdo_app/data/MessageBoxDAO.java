package com.playdo.playdo_app.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.playdo.playdo_app.model.MessageBox;

@Stateless
@LocalBean
public class MessageBoxDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<MessageBox> getAll() {
		Query query = em.createQuery("SELECT b FROM MessageBox b");
		return query.getResultList();
	}

	public MessageBox getById(int id) {
		return em.find(MessageBox.class, id);
	}

	@SuppressWarnings("unchecked")
	public List<MessageBox> getMessageBoxByReciver(int reciver) {
		final Query query = em.createQuery("SELECT b FROM MessageBox AS b " + 
										   "WHERE b.receiverId LIKE ?1");
		query.setParameter(1, reciver);
		return query.getResultList();
	}
	
	public void save(MessageBox mbox){
		em.persist(mbox);
	}

	public void update(MessageBox mbox) {
		em.merge(mbox);
	}

	public void delete(int id) {
		em.remove(getById(id));
	}

	


}
