package com.playdo.playdo_app.data;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.playdo.playdo_app.controller.Secure;
import com.playdo.playdo_app.model.Users;

@Stateless
@LocalBean
public class UsersDAO {

	@PersistenceContext
	private EntityManager em;

	@EJB
	private Secure secure;

	@SuppressWarnings("unchecked")
	public List<Users> getAll() {
		Query query = em.createQuery("SELECT u FROM Users AS u ");
		return query.getResultList();
	}

	public Users getById(int id) {
		return em.find(Users.class, id);
	}
	
	public Users getUserByEmail(String email) {
		final Query query = em.createQuery("SELECT u FROM Users AS u WHERE u.email LIKE ?1");
		query.setParameter(1, "%" + email.toUpperCase() + "%");
		return (Users) query.getSingleResult();
	}
	
/*	public Users getLoggedInUserByEmail(String email) {
		final Query query = em.createQuery("SELECT u FROM Users AS u WHERE u.email LIKE ?1");
		query.setParameter(1, "%" + email.toUpperCase() + "%");
		return (Users) query.getSingleResult();
	}*/

	@SuppressWarnings("unchecked")
	public List<Users> getUserByFirstname(String firstName) {
		final Query query = em.createQuery("SELECT u FROM Users AS u WHERE u.firstName LIKE ?1");
		query.setParameter(1, "%" + firstName.toUpperCase() + "%");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Users> getUserByLastName(String lastName) {
		final Query query = em.createQuery("SELECT u FROM Users AS u WHERE u.lastName LIKE ?1");
		query.setParameter(1, "%" + lastName.toUpperCase() + "%");
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Users> getUserByTeam(int teamId) {
		final Query query = em.createQuery("SELECT u FROM Users AS u WHERE u.team.teamId = ?1");
		query.setParameter(1, teamId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Users> getUserByCompany(int companyId) {
		final Query query = em.createQuery("SELECT u FROM Users AS u WHERE u.company.companyId = ?1");
		query.setParameter(1, companyId);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Users> getUserWithCompanyName() {
		final Query query = em.createQuery("SELECT u.userId, u.email, u.firstName, u.lastName, u.dob, u.role, "
				+ "u.password, u.company.companyName, u.company.companyId " + "FROM Users AS u");

		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public List<Users> getUserWithProjectTeamName() {
		final Query query = em.createQuery("SELECT u.userId, u.avatar, u.firstName, u.lastName, "
				+ "u.dob, u.role, u.pointsEarned, u.team.teamName, u.team.project.projectName, "
				+ "u.email, u.company.companyId, u.team.teamId,u.team.project.projectId " + "FROM Users AS u  ");

		return query.getResultList();
	}

	public void save(Users user) throws InvalidKeyException, NoSuchAlgorithmException {
		Users newUser = user;
		String salt = secure.generateSalt();

		String password = user.getPassword();

		newUser.setPassword(secure.hash(salt + password));
		newUser.setSalt(salt);
		em.persist(newUser);
	}

	public void update(Users user) {
		em.merge(user);
	}

	public void delete(int id) {
		em.remove(getById(id));
	}

/*	@SuppressWarnings("unchecked")
	public List<Users> getUserByEmail(String email) {
		final Query query = em.createQuery("SELECT u FROM Users AS u WHERE u.email LIKE ?1");
		query.setParameter(1, "%" + email.toUpperCase() + "%");
		return query.getResultList();
	}*/

	public Users checkLogin(final String email, final String password) {
		Users user = getUserByEmail(email);
	/*	List<Users> users = getUserByEmail(email);
		Users user = null;
		
		for (Users u : users) {
			if(email.equals(u.getEmail())){
				user = u;
			}
		}*/
		
		String salt = user.getSalt();
		String passHashed = secure.hash(salt + password);
		String passFromDB = user.getPassword();
		if (passHashed.equals(passFromDB) ) {
			return user;
		} else {
			throw new IllegalArgumentException("invalid login");
		}
	}

}
