package com.playdo.playdo_app.data;

import java.math.BigInteger;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.playdo.playdo_app.model.Message;

@Stateless
@LocalBean
public class MessageDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	@SuppressWarnings("unchecked")
	public List<Message> getAll() {
		Query query = em.createQuery("SELECT m FROM Message m");
		return query.getResultList();
	}

	public Message getById(BigInteger id) {
		return em.find(Message.class, id);
	}

	public void save(Message message){
		em.persist(message);
	}

	public void update(Message message) {
		em.merge(message);
	}

	public void delete(BigInteger id) {
		em.remove(getById(id));
	}

}
