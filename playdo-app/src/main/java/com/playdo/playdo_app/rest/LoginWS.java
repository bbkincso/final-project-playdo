package com.playdo.playdo_app.rest;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.simple.JSONObject;

import com.playdo.playdo_app.controller.Secure;
import com.playdo.playdo_app.data.UsersDAO;
import com.playdo.playdo_app.model.Users;

@Path("/login")
@Stateless
@LocalBean
public class LoginWS {

	@EJB
	private UsersDAO userDao;

	@EJB
	private Secure secure;

	@SuppressWarnings({ "finally", "unchecked" })
	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces({ MediaType.APPLICATION_JSON })
	public Response login(@FormParam("email") final String email, 
						  @FormParam("password") final String password) {
		
		String role = null;
		String token = null;
		int userId = 0;
		int companyId = 0;
		JSONObject jSonToUser = new JSONObject();

		try {
			System.out.println("------------------ LoginWS - login method try ------------------");
			final Users user = userDao.checkLogin(email, password);
			
			role = user.getRole();
			userId = user.getUserId();
			companyId = user.getCompany().getCompanyId();
			token = secure.generateToken();
			user.setToken(token);

			System.out.println("------------------ email: " + email + ", pass: " + password + " ------------------");
			System.out.println("------------------ Role: " + role + ", Token: " + token + " ------------------");
			System.out.println("------------------ UserId: " + userId + ", CompanyId: " + companyId + " ------------------");

		} catch (Exception e) {
			e.printStackTrace();
			role = "index";
			userId = 0;
			companyId = 0;
			token = "";

			System.out.println("------------------ LoginWS - login method catch ------------------");
			System.out.println("------------------ email: " + email + ", pass: " + password + " ------------------");
			System.out.println("------------------ Role: " + role + ", Token: " + token + " ------------------");
			System.out.println("------------------ UserId: " + userId + ", CompanyId: " + companyId + " ------------------");

		} finally {
			
			jSonToUser.put("role", role);
			jSonToUser.put("token", token);
			jSonToUser.put("userId", userId);
			jSonToUser.put("companyId", companyId);
			final String json = jSonToUser.toJSONString();

			System.out.println("------------------ LoginWS - login method finally ------------------");
			return Response.ok(json, MediaType.APPLICATION_JSON).build();
		}
	}

	@GET
	@Path("/{role}")
	public String authorizeRole(@PathParam("role") final String role,
								@HeaderParam("hmac") final String hmac, 
								@HeaderParam("email") final String email) {
		
		Users user = userDao.getUserByEmail(email);
		
		if (user == null) {
			return "accessdenied";
		}

		String token = user.getToken();
		String serverHmac = Secure.getBase64EncodedHmac(email + role, token);

		System.out.println("------------------   LoginWS - authorizeRole method client hmac is " + hmac
				+ " server hmac is " + serverHmac + " token is " + token + "  ------------------");

		if (serverHmac.equals(hmac)) {
			return "accessgranted";
		}
		return "accessdenied";
	}
}
