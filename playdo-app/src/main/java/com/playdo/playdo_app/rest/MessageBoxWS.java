package com.playdo.playdo_app.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.playdo.playdo_app.data.MessageBoxDAO;
import com.playdo.playdo_app.model.MessageBox;

@Path("/messages")
@Stateless
public class MessageBoxWS {
	@EJB
	private MessageBoxDAO mBoxDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	public Response findAll() {
		List<MessageBox> mBoxes = mBoxDao.getAll();
		return Response.status(200).entity(mBoxes).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findMessageBoxById(@PathParam("id") int id ) {
		MessageBox mBox = mBoxDao.getById(id);
		return Response.status(200).entity(mBox).build();
	}
	
	@GET
    @Path("/search/{query}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getMessageBoxByReciver(@PathParam("query") int reciver){
    	List<MessageBox> mBoxes = mBoxDao.getMessageBoxByReciver(reciver);
    	return Response.status(200).entity(mBoxes).build();
    }
    
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response saveMessageBox(MessageBox mBox) {	
		mBoxDao.save(mBox);
		return Response.status(201).entity(mBox).build();
	}
	
	@PUT
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updateMessageBox(MessageBox mBox) {
		mBoxDao.update(mBox);
		return Response.status(200).entity(mBox).build();
		}
	
	@DELETE
	@Path("/{id}")
	public Response deleteMessageBox(@PathParam("id") int id) {
		mBoxDao.delete(id);
		return Response.status(204).build();
	}


}
