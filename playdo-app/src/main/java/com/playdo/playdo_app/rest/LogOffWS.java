package com.playdo.playdo_app.rest;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.playdo.playdo_app.controller.Secure;
import com.playdo.playdo_app.data.UsersDAO;
import com.playdo.playdo_app.model.Users;

@Path("/logoff")
@Stateless
@LocalBean
public class LogOffWS {

	@EJB
	private UsersDAO userDao;

	@EJB
	private Secure secure;

	@POST
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces({ MediaType.APPLICATION_JSON })
	public Response logout(@FormParam("hmac") final String hmac, 
						   @FormParam("email") final String email) {
		
		System.out.println("----------- in logOffWS logout method -----------");
		
		if (hmac == null || email == null) {
			return Response.status(404).build();
		}
		
		/*List<Users> users = userDao.getUserByEmail(email);
		Users user = null;

		for (Users u : users) {
			if (email.equals(u.getEmail())) {
				user = u;
			}
		}*/

		Users user = userDao.getUserByEmail(email);
		
		if (user == null) {
			System.out.println("----------- in logOffWS logout method user == null -----------");
			return Response.status(404).build();
		}

		String secretKeyString = user.getToken();
		String serverHmac = Secure.getBase64EncodedHmac(email, secretKeyString);
		
		if (serverHmac.equals(hmac)) {
			System.out.println("----------- in logOffWS logout method serverHmac.equals(hmac)  -----------");
			user.setToken("");
			return Response.status(200).build();
		}
		System.out.println("----------- in logOffWS logout method NOT serverHmac.equals(hmac)  -----------");
		return Response.status(404).build();

	}

}
