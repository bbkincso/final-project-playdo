package com.playdo.playdo_app.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.playdo.playdo_app.data.CompanyDAO;
import com.playdo.playdo_app.model.Company;

@Path("/company")
@Stateless
@LocalBean
public class CompanyWS {

	@EJB
	private CompanyDAO companyDao; 
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllCompanies() {
		List<Company> companies = companyDao.getAll();
		return Response.status(200).entity(companies).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Response findCompanyById(@PathParam("id") int id) {
		Company company = companyDao.getById(id);
		return Response.status(200).entity(company).build();
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveCompany(Company company) {
		companyDao.save(company);
		return Response.status(201).entity(company).build();
	}

	@PUT
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateCompany(Company company) {
		companyDao.update(company);
		return Response.status(200).entity(company).build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteCompany(@PathParam("id") int id) {
		companyDao.delete(id);
		return Response.status(204).build();
	}
}
