package com.playdo.playdo_app.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.playdo.playdo_app.data.TeamDAO;
import com.playdo.playdo_app.model.Team;

@Path("/team")
@Stateless
public class TeamWS {
	
	@EJB
	private TeamDAO teamDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	public Response findAll() {
		List<Team> teams = teamDao.getAll();
		return Response.status(200).entity(teams).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findTeamById(@PathParam("id") int id ) {
		Team team = teamDao.getById(id);
		return Response.status(200).entity(team).build();
	}
	
	@GET
    @Path("search/{query}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTeamByTeamName(@PathParam("query") String query){
    	List<Team> teams = teamDao.getTeamByTeamName(query);
    	return Response.status(200).entity(teams).build();
    }
	
/*	@GET
    @Path("project")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTeamWithProjectName(){
    	List<Team> teams = teamDao.getTeamWithProjectName();
    	return Response.status(200).entity(teams).build();
    }*/
    
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response saveTeam(Team team) {	
		teamDao.save(team);
		return Response.status(201).entity(team).build();
	}
	
	@PUT
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updateTeam(Team team) {
		teamDao.update(team);
		return Response.status(200).entity(team).build();
		}
	
	@DELETE
	@Path("/{id}")
	public Response deleteTeam(@PathParam("id") int id) {
		teamDao.delete(id);
		return Response.status(204).build();
	}

}
