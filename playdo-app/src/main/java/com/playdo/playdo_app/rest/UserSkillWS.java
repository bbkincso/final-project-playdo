package com.playdo.playdo_app.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
//import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
//import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.playdo.playdo_app.data.UserSkillDAO;
import com.playdo.playdo_app.model.UserSkill;

@Path("/userskills")
@Stateless

public class UserSkillWS {

	@EJB
	private UserSkillDAO userSkillDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	public Response findAll() {
		List<UserSkill> skills = userSkillDao.getAll();
		return Response.status(200).entity(skills).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findUser_SkillById(@PathParam("id") int id ) {
		UserSkill skill = userSkillDao.getById(id);
		return Response.status(200).entity(skill).build();
	}
	
	@GET
    @Path("user/{query}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSkillByUserId(@PathParam("query") int query){
    	List<UserSkill> skill = userSkillDao.getSkillByUserId(query);
    	return Response.status(200).entity(skill).build();
    }
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response saveUserSkill(UserSkill skill) {	
		userSkillDao.save(skill);
		return Response.status(201).entity(skill).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteUserSkill(@PathParam("id") int id) {
		userSkillDao.delete(id);
		return Response.status(204).build();
	}
}
