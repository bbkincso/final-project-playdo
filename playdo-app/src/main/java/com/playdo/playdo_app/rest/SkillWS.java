package com.playdo.playdo_app.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.playdo.playdo_app.data.SkillDAO;
import com.playdo.playdo_app.model.Skill;

@Path("/skill")
@Stateless
@LocalBean
public class SkillWS {
	
	@EJB
	private SkillDAO skillDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	public Response findAll() {
		List<Skill> skills = skillDao.getAll();
		return Response.status(200).entity(skills).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findSkillById(@PathParam("id") int id ) {
		Skill skill = skillDao.getById(id);
		return Response.status(200).entity(skill).build();
	}
	
	@GET
    @Path("search/{query}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getSkillBySkillName(@PathParam("query") String query){
    	List<Skill> skills = skillDao.getSkillBySkillName(query);
    	return Response.status(200).entity(skills).build();
    }
    
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response saveSkill(Skill skill) {	
		skillDao.save(skill);
		return Response.status(201).entity(skill).build();
	}
	
	@PUT
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updateSkill(Skill skill) {
		skillDao.update(skill);
		return Response.status(200).entity(skill).build();
		}
	
	@DELETE
	@Path("/{id}")
	public Response deleteSkill(@PathParam("id") int id) {
		skillDao.delete(id);
		return Response.status(204).build();
	}

}
