package com.playdo.playdo_app.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.playdo.playdo_app.data.ProjectDAO;
import com.playdo.playdo_app.model.Project;

@Path("/project")
@Stateless
@LocalBean
public class ProjectWS {
	
	@EJB
	private ProjectDAO projectDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	public Response findAll() {
		List<Project> projects = projectDao.getAll();
		return Response.status(200).entity(projects).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findProjectById(@PathParam("id") int id ) {
		Project project = projectDao.getById(id);
		return Response.status(200).entity(project).build();
	}
	
	@GET
    @Path("search/{query}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProjectByProjectName(@PathParam("query") String query){
    	List<Project> projects = projectDao.getProjectByProjectName(query);
    	return Response.status(200).entity(projects).build();
    }
	
	@GET
    @Path("company/{id}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getProjectByCompanyId(@PathParam("id") String id){
    	List<Project> projects = projectDao.getProjectByProjectName(id);
    	return Response.status(200).entity(projects).build();
    }
    
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response saveProject(Project project){	
		projectDao.save(project);
		return Response.status(201).entity(project).build();
	}
	
	@PUT
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updateProject(Project project) {
		projectDao.update(project);
		return Response.status(200).entity(project).build();
		}
	
	@DELETE
	@Path("/{id}")
	public Response deleteProject(@PathParam("id") int id) {
		projectDao.delete(id);
		return Response.status(204).build();
	}

}
