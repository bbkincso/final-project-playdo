package com.playdo.playdo_app.rest;

import java.math.BigInteger;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.playdo.playdo_app.data.MessageDAO;
import com.playdo.playdo_app.model.Message;

@Path("/message")
@Stateless
@LocalBean
public class MessageWS {
	
	@EJB
	private MessageDAO messageDao;

	@GET
	@Produces({ MediaType.APPLICATION_JSON})
	public Response findAll() {
		List<Message> messages = messageDao.getAll();
		return Response.status(200).entity(messages).build();
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{id}")
	public Response findMessageById(@PathParam("id") BigInteger id ) {
		Message message = messageDao.getById(id);
		return Response.status(200).entity(message).build();
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response saveMessage(Message message) {	
		messageDao.save(message);
		return Response.status(201).entity(message).build();
	}
	
	@PUT
	@Consumes("application/json")
	@Produces({MediaType.APPLICATION_JSON})
	public Response updateMessage(Message message) {
		messageDao.update(message);
		return Response.status(200).entity(message).build();
		}
	
	@DELETE
	@Path("/{id}")
	public Response deleteMessage(@PathParam("id") BigInteger id) {
		messageDao.delete(id);
		return Response.status(204).build();
	}
}
