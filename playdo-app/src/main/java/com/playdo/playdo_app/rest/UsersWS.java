package com.playdo.playdo_app.rest;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.playdo.playdo_app.data.UsersDAO;
import com.playdo.playdo_app.model.Users;

@Path("/users")
@Stateless
@LocalBean
public class UsersWS {

	@EJB
	private UsersDAO userDao;

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAll() {
		List<Users> users = userDao.getAll();
		return Response.status(200).entity(users).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Response findUsersById(@PathParam("id") int userId) {
		Users user = userDao.getById(userId);
		return Response.status(200).entity(user).build();
	}

	@GET
	@Path("company")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getUserWithCompanyName() {
		List<Users> users = userDao.getUserWithCompanyName();
		return Response.status(200).entity(users).build();
	}

	@GET
	@Path("project/team")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getUserWithProjectTeamName() {
		List<Users> users = userDao.getUserWithProjectTeamName();
		return Response.status(200).entity(users).build();
	}
	
	@GET
	@Path("loggedin/{query}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response getUserByEmail(@PathParam("query") String query) {
		Users user = userDao.getUserByEmail(query);
		return Response.status(200).entity(user).build();
	}

	@GET
	@Path("search/{query}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findByFirstName(@PathParam("query") String query) {
		List<Users> users = userDao.getUserByFirstname(query);
		return Response.status(200).entity(users).build();
	}

	@GET
	@Path("search/{query}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findByLastName(@PathParam("query") String query) {
		List<Users> users = userDao.getUserByLastName(query);
		return Response.status(200).entity(users).build();
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveUsers(Users user) throws InvalidKeyException, NoSuchAlgorithmException {
		userDao.save(user);
		return Response.status(201).entity(user).build();
	}

	@PUT
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateUsers(Users user) {
		userDao.update(user);
		return Response.status(200).entity(user).build();
	}

	@DELETE
	@Path("/{id}")
	public Response deleteUsers(@PathParam("id") int id) {
		userDao.delete(id);
		return Response.status(204).build();
	}
}
