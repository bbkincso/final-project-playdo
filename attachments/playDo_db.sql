DROP DATABASE IF EXISTS db_PlayDo;
CREATE DATABASE IF NOT EXISTS db_PlayDo;
USE db_PlayDo;


DROP TABLE IF EXISTS Company;
CREATE TABLE Company(
	companyId integer AUTO_INCREMENT UNIQUE NOT NULL,
	companyName VARCHAR(30) UNIQUE NOT NULL,
	PRIMARY KEY(companyId)
 );
 
INSERT INTO Company(companyId, companyName)
VALUES(null, 'PlayDO');
INSERT INTO Company(companyId, companyName)
VALUES(null,'First Company');
INSERT INTO Company(companyId, companyName)
VALUES(null,'Second Company');
INSERT INTO Company(companyId, companyName)
VALUES(null,'Third Company');
INSERT INTO Company(companyId, companyName)
VALUES(null,'Fourth Company');

DROP TABLE IF EXISTS Project;
CREATE TABLE Project(
	projectId integer AUTO_INCREMENT UNIQUE NOT NULL,
	projectName VARCHAR(30) UNIQUE NOT NULL,
    startDate VARCHAR(10) NOT NULL,
	endDate VARCHAR(10) NOT NULL,
	companyId integer NOT NULL,
	PRIMARY KEY(projectId),
    FOREIGN KEY (companyId) REFERENCES Company(companyId)
 );
 
INSERT INTO Project(projectId, projectName, startDate, endDate, companyId)
VALUES( null,'First Company Project 1', '03/03/2018', '03/04/2018', 2);
INSERT INTO Project(projectId, projectName, startDate, endDate, companyId)
VALUES(null,'First Company Project 2', '04/06/2018', '02/08/2018', 2);
INSERT INTO Project(projectId, projectName, startDate, endDate, companyId)
VALUES(null,'First Company Project 3', '13/09/2018', '13/10/2018', 2);
INSERT INTO Project(projectId, projectName, startDate, endDate, companyId)
VALUES( null,'Second Company Project 1', '02/03/2018', '03/05/2018', 3);
INSERT INTO Project(projectId, projectName, startDate, endDate, companyId)
VALUES( null,'Second Company Project 2', '03/04/2018', '12/07/2018', 3);


DROP TABLE IF EXISTS Team;
CREATE TABLE Team(
	teamId integer AUTO_INCREMENT UNIQUE NOT NULL,
	teamName VARCHAR(30) UNIQUE NOT NULL,
    pointsEarned integer,
    projectId integer,
	PRIMARY KEY(teamId),
	FOREIGN KEY (projectId) REFERENCES Project(projectId)
 );
 
INSERT INTO Team(teamId, teamName, pointsEarned, projectId)
VALUES(null, 'First Company Team 1', 0, 1);
INSERT INTO Team(teamId, teamName, pointsEarned, projectId)
VALUES(null, 'First Company Team 2', 0, 1);
INSERT INTO Team(teamId, teamName, pointsEarned, projectId)
VALUES(null, 'First Company Team 3', 0, 2);
INSERT INTO Team(teamId, teamName, pointsEarned, projectId)
VALUES(null, 'First Company Team 4', 0, 2);
INSERT INTO Team(teamId, teamName, pointsEarned, projectId)
VALUES(null, 'First Company Team 5', 0, 3);
INSERT INTO Team(teamId, teamName, pointsEarned, projectId)
VALUES(null, 'Second Company Team 1', 0, 4);
INSERT INTO Team(teamId, teamName, pointsEarned, projectId)
VALUES(null, 'Second Company Team 2', 0, 4);
INSERT INTO Team(teamId, teamName, pointsEarned, projectId)
VALUES(null, 'Second Company Team 3', 0, 4);



DROP TABLE IF EXISTS Users;
CREATE TABLE Users(
	userId INTEGER auto_increment NOT NULL UNIQUE,
	email VARCHAR(50) UNIQUE NOT NULL,
	password VARCHAR(255) NOT NULL,
    salt VARCHAR(255) NOT NULL,
    firstName VARCHAR(30) NOT NULL,
    lastName VARCHAR(30) NOT NULL,
    dob VARCHAR(30) NOT NULL,
    pointsEarned integer,
    role VARCHAR(30) NOT NULL,
    token VARCHAR(255),
    avatar VARCHAR(200),
    teamId integer,
    companyId integer NOT NULL,
    PRIMARY KEY(userId),
    FOREIGN KEY (teamId) REFERENCES Team(teamId),
    FOREIGN KEY (companyId) REFERENCES Company(companyId)
 );
 
 INSERT INTO Users (userId, email, password, salt, firstName, lastName, dob, pointsEarned, role, avatar, token, teamId, companyId)
 VALUES (null, "su@gmail.com", "4KCvGl9F0dmVzlzfib4zcQ==", "qu85zJSCkukBo/LEIyZIC7fQLeY=", "John", "Doe", "01/01/2000", 0,  "superuser", "", "", null, 1 );
 INSERT INTO Users (userId, email, password, salt, firstName, lastName, dob, pointsEarned, role, avatar, token, teamId, companyId)
 VALUES (null, "admin@firstcompany.com", "p7wFZCJb7xWqkWuaJUQ6vA==", "BFoM5wCBsVbZohncZU1bHORAebo=", "Joe", "Bloggs", "01/01/2000", 0,  "admin", "", "resources/images/avatar/user.jpg", null, 2 );
 INSERT INTO Users (userId, email, password, salt, firstName, lastName, dob, pointsEarned, role, avatar, token, teamId, companyId)
 VALUES (null, "admin@secondcompany.com", "heUYPWsuE9ccecZNm5QAPQ==", "I/FCSVCcfTR88CsO++jrQeoO1j4=", "Jane", "Doe", "01/01/2000", 0,  "admin", "", "resources/images/avatar/user.jpg", null, 3 );
 
 

DROP TABLE IF EXISTS MessageBox;
CREATE TABLE MessageBox(
	messageBoxId integer auto_increment UNIQUE NOT NULL,
    closedBySender BOOLEAN default false,
    closedByReceiver BOOLEAN default false,
    senderId INTEGER,
    receiverId INTEGER,
    messageId bigint,
    isNew boolean default true,
    PRIMARY KEY(messageBoxId),
    FOREIGN KEY (senderId) REFERENCES Users(userId),
    FOREIGN KEY (messageId) REFERENCES Message(messageId)
 );


DROP TABLE IF EXISTS Message;
CREATE TABLE Message(
	messageId bigint UNIQUE NOT NULL,
    date VARCHAR(50),
	message TEXT NOT NULL,
    messageType VARCHAR(300) NOT NULL,
	PRIMARY KEY(messageId)
 );

DROP TABLE IF EXISTS Skill;
CREATE TABLE Skill(
	skillId integer auto_increment UNIQUE NOT NULL,
	skillName VARCHAR(30) NOT NULL,
    companyId integer NOT NULL,
	PRIMARY KEY(skillId)
 );
 
INSERT INTO Skill(skillId, skillName,companyId)
VALUES(null,'first skill', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'Java novice', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'Java tutor', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'HTML novice', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'HTML tutor', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'mysql novice', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'mysql tutor', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'CSS novice', 2);
INSERT INTO Skill(skillId, skillName,companyId)
VALUES(null,'CSS tutor', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'AJAX novice', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'AJAX tutor', 2);
INSERT INTO Skill(skillId, skillName, companyId)
VALUES(null,'akseugauighaiehgeariohgoie', 4);


DROP TABLE IF EXISTS UserSkill;
CREATE TABLE UserSkill(
	id integer auto_increment UNIQUE NOT NULL,
	userId integer NOT NULL,
	skillId integer NOT NULL,
	PRIMARY KEY(id),
	FOREIGN KEY (userId) REFERENCES Users(userId),
    FOREIGN KEY (skillId) REFERENCES Skill(skillId)
 );
 
 INSERT INTO UserSkill(id, userId, skillId)
VALUES(null,4, 5);
INSERT INTO UserSkill(id, userId, skillId)
VALUES(null,4, 3);
 